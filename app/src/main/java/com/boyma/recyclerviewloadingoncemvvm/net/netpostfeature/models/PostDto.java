package com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostDto {


    @SerializedName("userId")
    @Expose
    private int userId;
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("body")
    @Expose
    private String body;

    public String getTitle() {
        return title;
    }
    public int getUserId() {
        return userId;
    }
}
