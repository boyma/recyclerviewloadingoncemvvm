package com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature;

import com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.models.PostDto;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface Api {

    @GET("/posts")
    Single<List<PostDto>> getPosts();
}
