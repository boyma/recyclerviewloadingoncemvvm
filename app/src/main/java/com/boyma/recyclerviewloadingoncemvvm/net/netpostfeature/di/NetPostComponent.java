package com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.di;


import com.boyma.recyclerviewloadingoncemvvm.MainActivity;
import com.boyma.recyclerviewloadingoncemvvm.app.di.NetComponent;
import com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.IPostDataRepository;

import dagger.Component;

@PerMainActivityScope
@Component(modules = {NetPostModule.class},  dependencies = NetComponent.class)
public interface NetPostComponent {

    void injecttim(MainActivity mainActivity);

    IPostDataRepository getIPostDataRepository();
}
