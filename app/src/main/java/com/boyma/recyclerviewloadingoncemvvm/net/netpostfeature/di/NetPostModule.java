package com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.di;


import com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.Api;
import com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.IPostDataRepository;
import com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.PostDataRepositoryImpl;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class NetPostModule {

    @Provides
    @PerMainActivityScope
    public IPostDataRepository provideRepository(Api api){
        return new PostDataRepositoryImpl(api);
    }

    @Provides
    @PerMainActivityScope
    public Api provideApi(Retrofit retrofit){
        System.out.println("Api Constructor");
        return retrofit.create(Api.class);
    }

}
