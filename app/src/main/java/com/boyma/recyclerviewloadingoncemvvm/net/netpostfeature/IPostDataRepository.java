package com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature;

import com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.models.PostDto;

import java.util.List;

import io.reactivex.Single;

public interface IPostDataRepository {
    Single<List<PostDto>> getPosts();
}
