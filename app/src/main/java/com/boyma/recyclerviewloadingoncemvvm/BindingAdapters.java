package com.boyma.recyclerviewloadingoncemvvm;

import android.databinding.BindingAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class BindingAdapters {

    @BindingAdapter({"app:setLinAdapter"})
    public static void setLinAdapter(RecyclerView view, RecyclerView.Adapter adapter) {
        view.setLayoutManager(new LinearLayoutManager(view.getContext()));
        //Toast.makeText(view.getContext(),"asdasd",Toast.LENGTH_LONG).show();
        view.setAdapter(adapter);
    }
}
