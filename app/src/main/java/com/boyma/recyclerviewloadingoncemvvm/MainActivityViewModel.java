package com.boyma.recyclerviewloadingoncemvvm;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableField;

import com.boyma.recyclerviewloadingoncemvvm.app.App;
import com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.di.DaggerNetPostComponent;
import com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.di.NetPostComponent;
import com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.models.ItemType1;
import com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.models.ItemType2;
import com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.models.PostDto;
import com.github.nitrico.lastadapter.LastAdapter;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivityViewModel extends ViewModel {

    //ui
    public ObservableField<LastAdapter> recadapter = new ObservableField<>();
    //

    private CompositeDisposable subcriber = new CompositeDisposable();

    private NetPostComponent netPostComponent;
    private ArrayList<Object> al;

    public MainActivityViewModel() {
        initComponent();
        loadPosts();
    }


    @SuppressLint("CheckResult")
    private void loadPosts() {
        subcriber.add(netPostComponent.getIPostDataRepository().getPosts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        this::onSuccess,
                        throwable -> System.out.println(throwable.toString())
                ));

    }

    private void onSuccess(List<PostDto> postDtos) {
        al = extrudeList(postDtos);
        recadapter.set(new LastAdapter(al, BR.item)
                .map(ItemType1.class, R.layout.item_type1)
                .map(ItemType2.class, R.layout.item_type2));

        //deleteItem(0);

    }

    private ArrayList<Object> extrudeList(List<PostDto> postDtos) {
        ArrayList<Object> al = new ArrayList<>();
        for (PostDto dto : postDtos){
            if (dto.getUserId() == 1){
                al.add(new ItemType1(dto));
            }else {
                al.add(new ItemType2(dto));
            }
        }
        return al;
    }

    private void initComponent() {
        netPostComponent = DaggerNetPostComponent.builder()
                .netComponent((App.getNetComponent()))
                .build();
    }

    public void deleteItem(int i) {
        al.remove(i);
        al.remove(i);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        subcriber.clear();
    }
}
