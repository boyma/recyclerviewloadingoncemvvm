package com.boyma.recyclerviewloadingoncemvvm.app;

import android.app.Application;

import com.boyma.recyclerviewloadingoncemvvm.app.di.AppModule;
import com.boyma.recyclerviewloadingoncemvvm.app.di.DaggerNetComponent;
import com.boyma.recyclerviewloadingoncemvvm.app.di.NetComponent;
import com.boyma.recyclerviewloadingoncemvvm.app.di.NetModule;

public class App extends Application {

    private static NetComponent netComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        netComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule("http://jsonplaceholder.typicode.com/"))
                .build();

    }

    public static NetComponent getNetComponent() {
        return netComponent;
    }
}
